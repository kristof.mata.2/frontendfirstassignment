const bankMoneyElement = document.getElementById("bankMoney");
const loanElement = document.getElementById("loanButton");
const bankDebtTextElement = document.getElementById("deptText");
const bankDebtElement = document.getElementById("debt");

const workMoneyElement = document.getElementById("workMoney");
const workButtonElement = document.getElementById("workButton");
const sendBankElement = document.getElementById("sendBank");

const laptopsElement = document.getElementById("laptops");

const laptopNameElement = document.getElementById("laptopName");
const laptopDescElement = document.getElementById("laptopDesc");
const laptopSpecElement = document.getElementById("laptopSpec");
const laptopPriceElement = document.getElementById("laptopPrice");

const laptopImageElement = document.getElementById("laptopImage");
const buyButtonElement = document.getElementById("buyLaptop");



let bankMoney = 0;
let bankDebt = 0;

let workMoney = 0;

let laptops = [];
let specs = [];


bankMoneyElement.innerText = bankMoney;
bankDebtElement.innerText = bankDebt;
workMoneyElement.innerText = workMoney;
bankDebtTextElement.style.display='none';



const handleLoan = () => {
    const totalLoan = prompt("loan amount");
    if(Number.isFinite(Number(totalLoan))){
        if(bankDebt==0){
            if(Number(totalLoan)<=(bankMoney*2)){
                bankMoney = bankMoney + Number(totalLoan);
                bankDebt = bankDebt + Number(totalLoan);
                bankMoneyElement.innerText = bankMoney;
                bankDebtElement.innerText = bankDebt;
                if(bankDebt>0){
                    bankDebtTextElement.style.display='';
                }
            }
            else{
                alert(`Loan cant exceed two times your current balance: ${bankMoneyElement.innerText*2}.`);
            }
        }
        else{
            alert("Pay your debts first!")
        }
    }
    else{
        alert("Not a number.")
    }
}

const handleWork = () => {
    workMoney = workMoney + 100;
    workMoneyElement.innerText = workMoney;
}

const handleTransferToBank = () => {
    if(bankDebt>0){
        bankDebt = bankDebt-(workMoney*0.1)
        bankMoney = bankMoney + (workMoney*0.9);
    }
    else{
        bankMoney = bankMoney + workMoney;
    }

    workMoney = 0;
    workMoneyElement.innerText = workMoney;
    bankMoneyElement.innerText = bankMoney;
    bankDebtElement.innerText = bankDebt;
}

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToList(laptops))

const addLaptopsToList = (laptops) => {
    laptops.forEach(x => addLaptopToList(x));
    laptopNameElement.innerText = `${laptops[0].title}`;
    laptopDescElement.innerText = `${laptops[0].description}`;
    laptops[0].specs.forEach(x => addSpecsToList(x))
    laptopPriceElement.innerText = `${laptops[0].price}`;
}

const addLaptopToList = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopSelect = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    laptopImageElement.src = selectedLaptop.image;
    laptopNameElement.innerText = `${selectedLaptop.title}`;
    laptopDescElement.innerText = `${selectedLaptop.description}`;
    laptopSpecElement.innerText = "";
    selectedLaptop.specs.forEach(x => addSpecsToList(x));
    laptopPriceElement.innerText = `${selectedLaptop.price}`;
    
}

const addSpecsToList = (spec) => {
    const listItem = document.createElement("li");
    listItem.innerText = `${spec}`;
    laptopSpecElement.appendChild(listItem);
}

const handleZeroLoan = () => {
    if(bankDebt==0){
        bankDebtTextElement.style.display='none';
    }
    else{
        bankDebtTextElement.style.display='';
    }
}

const handleBuying = e => {
    const selectedLaptop = laptops[laptopsElement.selectedIndex];
    if(bankMoney>=selectedLaptop.price){
        bankMoney-=selectedLaptop.price;
        bankMoneyElement.innerText = bankMoney;
        alert("Enjoy your new laptop!");
    }
    else{
        alert("You require more minerals!"); 
    }

}


loanElement.addEventListener("click",handleLoan);

workButtonElement.addEventListener("click",handleWork);
sendBankElement.addEventListener("click",handleTransferToBank);

buyButtonElement.addEventListener("click",handleBuying);

laptopsElement.addEventListener("change", handleLaptopSelect);
loanElement.addEventListener("change", handleZeroLoan);




