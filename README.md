## Name
Frontend First assignment.

## Description
A dynamic webpage using “vanilla” JavaScript. Banking, loan, work, laptop buying.

## Image
Actual design.
![image.png](./image.png)
Plan:
![image-1.png](./image-1.png)

## Installation
No need to install.

## Usage
Open the index.html.
Use the webpage with buttons and the scroll down menu.

## Maintainers
@kristof.mata.2

## Project status
Development has slowed down or stopped completely.
